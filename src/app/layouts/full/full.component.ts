import { Component, OnInit, HostListener, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { API_URLs } from '../../shared/models/constants';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit , AfterViewInit{
  //color = 'defaultdark';
  color = 'blue';
  showSettings = false;
  showMinisidebar = false;
  showDarktheme = false;

  @ViewChild('pageWrapper', {static: false}) pageWrapper : ElementRef;

  public innerWidth: any;

  public config: PerfectScrollbarConfigInterface = {};

  constructor(public router: Router , private apiUrls : API_URLs) {}

  ngOnInit() {
    console.log("in full component");
    
    if (this.router.url === '/') { 
      this.router.navigate(['/dashboard/dashboard1']);
    }
    this.handleLayout();
  }

  ngAfterViewInit(){
    this.apiUrls.pageWrapperId = this.pageWrapper.nativeElement.id;
    console.log("id ==== "+this.pageWrapper.nativeElement.id);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.handleLayout();
  }

  toggleSidebar() {
    this.showMinisidebar = !this.showMinisidebar;
  }

  handleLayout() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 576) {
      this.showMinisidebar = true;
    } else {
      this.showMinisidebar = false;
    }
  }
}
