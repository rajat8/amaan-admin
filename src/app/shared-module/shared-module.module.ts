import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModuleRoutingModule } from './shared-module-routing.module';
import { DateFormatPipe } from './dateformatpipe/customdateformat.pipe';
import { ModalModule} from 'ngx-bootstrap/modal';
import { AlertComponent } from './alert/alert.component';

import { FileformatdetailsComponent } from './fileformatdetails/fileformatdetails.component';
import { MultiSelectComponent } from './multi-select/multi-select.component'
import { NgSelect2Module } from 'ng-select2';
@NgModule({
  imports: [
    CommonModule,
    SharedModuleRoutingModule,
    NgSelect2Module,
    ModalModule.forRoot()
  ],
  declarations: [DateFormatPipe , AlertComponent, FileformatdetailsComponent, MultiSelectComponent],
  exports:[DateFormatPipe , AlertComponent, FileformatdetailsComponent, MultiSelectComponent],
  providers:[
    
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class SharedModule { }
