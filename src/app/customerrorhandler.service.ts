import {ErrorHandler, ApplicationRef, Injector} from '@angular/core';
import { Injectable,Inject } from '@angular/core';
import { Router,ActivatedRoute, ROUTES, ROUTER_INITIALIZER, ROUTER_CONFIGURATION } from '@angular/router';

// import { RoleTypeService } from './roletype/roletypeservice/roletype.service';
import { AuthenticationService } from './authentication/service/authentication.service';

@Injectable()
export class CustomErrorHandler implements ErrorHandler {    
//   constructor(private router : Router , private activatedrouter : ActivatedRoute) {}
  private router:Router;
  private authenticationService:AuthenticationService;
  constructor(private injector: Injector) {}
  
  handleError(error) {
    // your custom error handling logic    
    console.log(error);
    this.authenticationService = this.injector.get(AuthenticationService);
    this.authenticationService.storage = error;

    this.router = this.injector.get(Router);
    this.router.navigate(["/authentication/404"],{queryParams:{code : 303}});
    
  }

}