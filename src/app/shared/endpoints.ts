export class Endpoints {

    // API's
    //public static API_ENDPOINT='http://localhost:8181';  // main
    public static API_ENDPOINT = "http://180.179.201.99:8181";
    // public static API_ENDPOINT='http://180.179.201.99:8181';  // main
    public static REPORT_API_ENDPOINT = 'http://180.179.201.99:8183';  // main
    //public static REPORT_API_ENDPOINT='http://localhost:8080';  // main


    //'http://180.179.201.99:8183'
    // public static API_ENDPOINT='http://192.168.0.22:8181';
    // public static API_ENDPOINT='http://192.168.0.36:8181';  // palak
    public static USERS_API = '/api/users/';
    public static RIGHTS_API = '/api/rights';
    public static VALIDATE_ROLE_CODE = "/api/checkRoleCode";
    public static CREATE_ROLE_CODE = "/api/createRole";
    public static SEARCH_ROLE_LIST = "/api/searchRole";
    public static LOGIN_API = '/api/login';
    public static AUTH_API = '/oauth/token';
    public static VIEW_ROLE = "/api/viewRole";
    public static SEARCH_DISTRIBUTOR_COMPANY = "/api/searchDtrCompany";
    public static SEARCH_DISTRIBUTOR_USER_LIST = "/api/searchDistributorUser";
    public static SEARCH_DISTRIBUTOR_NODE_USER_LIST = "/api/searchDistributorNodeUser";
    public static SEARCH_RETAILER_USER_LIST = "/api/searchRetailerUser";
    public static VIEW_DISTRIBUTOR_COMPANY = "/api/viewDistributorCompany";
    public static VIEW_DISTRIBUTOR_NODE_COMPANY = "/api/viewDistributorNodeCompany";
    public static VIEW_DISTRIBUTOR_USER = "/api/viewDistributorUser";
    public static VIEW_DISTRIBUTOR_NODE_USER = "/api/viewDistributorNodeUser";
    public static SEARCH_DISTRIBUTOR_NODE_COMPANY = "/api/searchDtrNodeCompany";
    public static SEARCH_RETAILER_COMPANY = "/api/searchRtrCompany";
    public static VIEW_RETAILER_USER = "/api/viewRetailerUser"
    public static VIEW_COMPANY_ADDRESS = "/api/viewCompanyAddress";
    public static VIEW_RETAILER_COMPANY = "/api/viewRetailerCompany";
    public static VIEW_USER_ADDRESS = "/api/viewUserAddress"
    public static VIEW_WALLET = "/api/viewWallet"
    public static GET_REFILL_MASTER_BALANCE = "/api/refillMasterBalance"
    public static GET_REFILL_BALANCE = "/api/refillBalance"
    public static GET_REFILL_BONUS = "/api/refillBonus"
    public static REFILL_BALANCE_APPROVAl = "/api/refillBalanceApproval"
    public static REFILL_BONUS_APPROVAl = "/api/refillBonusApproval"
    public static REFILL_BAL_APPROVAl = "/api/refillSubmitApproval";
    public static REFILL_BAL_REJECTION = "/api/refillRejectionApproval";
    public static BALANCE_ADJUSTMENT_MASTER_BALANCE = "/api/balanceAdjustmentApi";
    public static SEARCH_TRANSACTION_BY_CRITERIA = "/api/searchTransactionApi"
    public static AUTOCONFIG = "/api/autoTransferApi";
    public static GETMASTERDATA = "/api/masterDataApi";
    public static VIEW_TRANSACTION = "/api/viewTransactionApi";
    public static PRODUCT_PAGE_DATA_API = "/api/productPageDataApi";
    public static VIEW_PRODUCT = "/api/viewProduct";
    public static VIEW_NOTIFICATION_LOW_BALANCE = "/api/viewLowBalanceNotify";
    public static NOTIFICATION_LOW_BALANCE = "/api/lowBalance";
    public static BATCH_RE_RUN_PROCESS = "/api/batchSearch";
    public static BATCH_RE_RUN_UPDATE = "/api/batchUpdate";
    public static CREATE_TYPE_WISE_NOTIFICATION = "/api/createTypeWiseNotify";
    public static CREATE_PRODUCT_API = "/api/createProduct";
    public static MODIFY_PRODUCT_API = "/api/modifyProduct";
    public static PRODUCT_PAGE = "/api/productPageDataApi";
    public static SEARCH_PRODUCT = "/api/searchProduct";
    public static CANCEL_TRANSACTION = "/api/cancelTransactionApi";
    public static CLONE_PRODUCT = "/api/cloneProduct";
    public static SEARCH_TRANSTYPE_NOTIFICATION_API = "/api/searchTypeWiseNotify";
    public static VIEW_TRANSTYPE_NOTIFICATION_API = "/api/viewTypeWiseNotify";
    public static RESET_PASSWORD = "/api/resetPwd";
    public static RESET_PIN = "/api/resetPin";
    public static MODIFY_TRANSTYPE_NOTIFICATION_API = "/api/modifyTypeWiseNotify"
    public static VIEW_GLOBAL_TRANS_TYPE_WISE = "/api/transViewGlobalLimit";
    public static CREATE_GLOBAL_TRANS_TYPE_WISE = "/api/transtypeGlobalLimit";
    public static MODIFY_SPECIPIC_LIMIT_API = "/api/modifySpecificLimit"
    public static VIEW_INTERNAL_USER = "/api/viewInternalUser";
    public static CHANGE_INTERNAL_USER_STATUS = "/api/changeInternalUserStatus";
    public static VIEW_GLOBAL_LIMIT = "/api/viewGlobalLimit";
    public static UPDATE_GLOBAL_LIMIT = "/api/updateglobalLimit";

    public static REPORT_API = "/report/userReport";
    public static REPORT_FILTER_API = "/report/filterData";
    public static REPORT_NEW_FILTER_API = "/report/newFilterData";
    public static MODIFY_INTERNAL_USER_API = "/api/modifyInternalUser";
    public static VIEW_INTERNAL_USER_API = "/api/viewInternalUser"
    public static SEARCH_INTERNAL_USER = "/api/searchInternalUser"
    public static CREATE_INTERNAL_USER_API = "/api/createInternalUser";
    public static BULK_SEARCH_ROLE_API = "/api/bulkSearchRole";
    public static ORS_SEARCH_PAGE = "/ors/service";
    public static ORS_EDIT_PAGE = "/ors/editRecord";
    public static ORS_EDIT_LOG_PAGE = "/ors/editLog";
    
    public static SEARCH_PROMOTION_CONDITION = "/api/promotionCondition/search";
    public static BULK_FILE_UPLOAD = "/api/bulkFileUpload";
    public static BULK_UPLOAD = "/api/bulkDownload";

    public static TRANSFER_BALANCE_FEE = "/api/searchTransferBalanceFee"
    public static MODIFY_TRANSFER_BALANCE_FEE = "/api/modifyTransferBalanceFee"

    public static CHANGE_PIN = "/api/changePin"
    public static CHANGE_PASS = "/api/changePwd"

    public static VIEWRISKLEVEL = "/api/viewRiskLevel";
    public static MODIFYRISKLEVEL = "/api/modifyRiskLevel";

    public static CREATE_APPLICATION = "/api/createAppCode";
    public static CREATE_APPLICATION_CHANNEL = "/api/createAppChannel";
    public static CREATE_PAYMENT_CHANNEL = "/api/createPymntChannel";
    public static CREATE_PRODUCT_TYPE = "/api/createProductType";
    public static CREATE_PROMOTION_CONDITION = "/api/promotionCondition/create";
    public static SEARCH_APPLICATION = "/api/searchAppCode";
    public static SEARCH_APPLICATION_CHANNEL = "/api/searchAppChannel";
    public static SEARCH_PAYMENT_CHANNEL = "/api/searchPaymentChannel";
    public static SEARCH_PRODUCT_TYPE = "/api/searchProductType ";

    public static MODIFY_APP_CODE = "/api/modifyAppCode";
    public static MODIFY_APP_CHANNEL = "/api/modifyAppChannel";
    public static MODIFY_PAYMENT_CHANNEL = "/api/modifyPymntChannel";
    public static MODIFY_PRODUCT_TYPE = "/api/modifyProductType";

    public static VIEW_APP_CODE = "/api/viewAppCode";
    public static VIEW_APP_CHANNEL = "/api/viewAppChannel";
    public static VIEW_PAYMENT_CHANNEL = "/api/viewPymntChannel";
    public static VIEW_PRODUCT_TYPE = "/api/viewProductType";

    public static SEARCH_APP_CODE = "/api/searchAppCode";
    public static CLONE_PROMOTION = "/api/clonePromotion";
    public static IWP_NEW_FILTER_API = "/api/newFilterData";
    public static SEARCH_PROMOTIONS ="/api/searchPromtion"
    public static CREATE_PROMOTIONS ="/api/createPromtion"
    public static VIEW_PROMOTIONS ="/api/viewPromtion"

    // Pattern Validations
    //public static pattern= "^[0-9a-zA-Z]+[^*$&%><]*[0-9a-zA-Z]$";
    public static codeNumericPattern = "^[0-9]*"
    public static codeNumericPatternAmount = "^[0-9.,]*"
    public static numericdecimalPatternAmount = "^([0-9]{0,12})+([.][0-9]{1,2})?$"
    public static numericdecimalwithComamPatternAmount = "^([0-9,]{0,12})+([.][0-9]{1,2})?$"
    public static numericdecimalPatternAmountWithNegative = "^([-0-9]{0,12})+([.][0-9]{1,2})?$"
    public static codePattern = "^[0-9\u0E00-\u0E7Fa-zA-Z]+[0-9\u0E00-\u0E7Fa-zA-Z_-]*[0-9\u0E00-\u0E7Fa-zA-Z]$";
    //public static namePattern= "^[0-9a-zA-Z]+([^*$&%><]*[0-9a-zA-Z])*";
    public static namePattern = "^[0-9\u0E00-\u0E7Fa-zA-Z]+[^*$&%><]*[0-9\u0E00-\u0E7Fa-zA-Z]$";
    public static alphaNumericPattern = '^[\u0E00-\u0E7Fa-zA-Z0-9]*';
    public static notificationPattern = "^[^*$&%><]+$";
    public static emailPattern = "^[_\u0E00-\u0E7FA-Za-z0-9-\\._]*@[\u0E00-\u0E7FA-Za-z0-9-]*(\\.[\u0E00-\u0E7FA-Za-z]{2,})$";
    public static namePatternTest = "^([0-9\u0E00-\u0E7Fa-zA-Z][^*$&%><]?)*[0-9\u0E00-\u0E7Fa-zA-Z]$";
    public static ipPattern = "^[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
}

