import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class LoginLogoutAuthenticationService{
    constructor(private http : HttpClient){}
   
    logIn(url: string, request: any) {
        console.log(url + '  ' + request);
        return this.http.post<any>(url, request)
            .toPromise()
            .then(data => {     
                return data
            }, error => {
                console.log("API error : " + JSON.stringify(error));
                return null;
            }
            );
    }

    logOut(url: string, request: any) {
        return this.http.post<any>(url, request)
            .toPromise()
            .then(data => {     
                return data
            }, error => {
                console.log("API error : " + JSON.stringify(error));
                return null;
            }
            );
    }

    findActionPriority(levelList:any[],subLevelIdLocal:string){
        var subLevel;
        levelList.filter((element) => (element.level.find(level=> level.subLevel.find(function(sub){if(sub.roleId == subLevelIdLocal)subLevel=sub}))))    
        return subLevel.actionPriority.split(",");
    }

    
}