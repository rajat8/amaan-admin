import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
 
  {
    path: '',
    title: 'User',
    icon: 'mdi mdi-account',
    class: '',
    label: '3',
    labelClass: 'label label-rouded label-themecolor pull-right',
    extralink: false,
    submenu: [
    ]
  }
   
];
