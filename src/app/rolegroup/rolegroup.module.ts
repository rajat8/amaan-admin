import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateRoleGroup } from './createrolegroup/createrolegroup.component';
import { RoleGroupRoutingModule } from './rolegroup-routing.module';
import { RoleGroupService } from './rolegroup.service';
import { RoleGroup} from './rolegroup.component';
import { ViewRoleGroup } from './viewrolegroup/viewrolegroup.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [ RoleGroup,CreateRoleGroup,ViewRoleGroup],
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      RoleGroupRoutingModule,
    ],
    providers: [
      RoleGroupService
    ]
  })
export class RoleGroupModule{}