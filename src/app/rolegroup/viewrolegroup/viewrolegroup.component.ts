import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder,FormControl } from '@angular/forms';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { RoleGroupService } from '../rolegroup.service';
import { RoleGroupModel } from '../rolegroup.model';

@Component({
  selector: 'app-viewrolegroup',
  templateUrl: './viewrolegroup.component.html',
  styleUrls: ['./viewrolegroup.component.css']
})
export class ViewRoleGroup implements OnInit {

  
  editMode = false;
  paramList :any;
  roleGroupModel: RoleGroupModel;
  id : number;
  code : String;
  name: String
  roleGrpParentCode : String;
  permissionCode : String;
  status : String;
  createdOn : Date;
  createdBy : String;
  updatedOn :  Date;
  updatedBy :String;

  constructor(private roleGroupService: RoleGroupService,private formBuilder: FormBuilder, private router: Router, private activatedrouter: ActivatedRoute,
    private route: ActivatedRoute) { }

   
  

  ngOnInit() {

      this.route.params.subscribe((params: Params) => {
       
    
      if(params.id) {
      console.log("params" + params.id);
        this.roleGroupModel = this.roleGroupService.geSystemUserById(+params.id);
        this.id =  this.roleGroupModel.id;
        this.code = this.roleGroupModel.code;
        this.name = this.roleGroupModel.name;
        this.roleGrpParentCode = this.roleGroupModel.roleGrpParentCode;
        this.permissionCode = this.roleGroupModel.permissionCode;
        this.createdOn = this.roleGroupModel.createdOn;
        this.createdBy = this.roleGroupModel.createdBy;
        this.status = this.roleGroupModel.status;
      }
     
       
    });
  }

  navigateToSytemUserCreation(){
    
      this.router.navigate( ['../../'], {relativeTo: this.route} );
    
  }
   

  onCancel() {
    
  
      this.router.navigate( ['../../'], {relativeTo: this.route} );
     
  }
 
}
