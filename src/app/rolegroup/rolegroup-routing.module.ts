import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
 
import { RoleGroup } from './rolegroup.component';
import { CreateRoleGroup } from './createrolegroup/createrolegroup.component';
import { ViewRoleGroup } from './viewrolegroup/viewrolegroup.component';
const routes: Routes = [
    { path: '', component: RoleGroup, pathMatch: "full" },
    { path: 'add', component: CreateRoleGroup },
    { path: 'view/:id', component: ViewRoleGroup },
    { path: 'edit/:id', component: CreateRoleGroup }
  ];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class RoleGroupRoutingModule{}