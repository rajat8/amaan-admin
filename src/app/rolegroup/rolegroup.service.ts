import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGroupModel } from './rolegroup.model';
import { Router } from '@angular/router';

@Injectable()
export class RoleGroupService {

    constructor(private router: Router) { }

    private roleGroupModel: RoleGroupModel[];
 

    public fetchSystemUserList(): RoleGroupModel[] {
        const systemUsers: RoleGroupModel[] = [

       
            {
                id: 101,
                code: '10001',
                name: 'Name1',
                createdOn: new Date(),
                roleGrpParentCode: 'RP0001',
                permissionCode: 'PC0001',
                createdBy :'ADM',
                updatedOn : new Date(),
                updatedBy: 'ADM',
                status : 'Inactive'
                
            },
            {
                id: 102,
                code: '10002',
                name: 'Name1',
                createdOn: new Date(),
                roleGrpParentCode: 'RP0002',
                permissionCode: 'PC0002',
                createdBy :'ADM',
                updatedOn : new Date(),
                updatedBy: 'ADM',
                status : 'Inactive'
                
            },
            {
                id: 103,
                code: '10003',
                name: 'Name1',
                createdOn: new Date(),
                roleGrpParentCode: 'RP0003',
                permissionCode: 'PC0003',
                createdBy :'ADM',
                updatedOn : new Date(),
                updatedBy: 'ADM',
                status : 'Inactive'
                
            },
            {
                id: 104,
                code: '10004',
                name: 'Name1',
                createdOn: new Date(),
                roleGrpParentCode: 'RP0004',
                permissionCode: 'PC0004',
                createdBy :'ADM',
                updatedOn : new Date(),
                updatedBy: 'ADM',
                status : 'Inactive'
                
            },
            {
                id: 105,
                code: '10005',
                name: 'Name1',
                createdOn: new Date(),
                roleGrpParentCode: 'RP0005',
                permissionCode: 'PC0005',
                createdBy :'ADM',
                updatedOn : new Date(),
                updatedBy: 'ADM',
                status : 'Inactive'
                
            },
             
            
            
        ];

        this.roleGroupModel = systemUsers;
        return systemUsers;
    }

    public geSystemUserById(systemUserId: number): RoleGroupModel {
        for (let i = 0; i < this.roleGroupModel.length; i++) {
            if (this.roleGroupModel[i].id === systemUserId) {
                return this.roleGroupModel[i];
            }
        }
       // return null;
    }

    public getCurrentURL() {
        return this.router.url;
    }

    public prepareUserAction(): boolean {
        const currentURL = this.getCurrentURL();
        if (currentURL.indexOf('/add') > -1) {
            return false;
        } else if (currentURL.indexOf('/edit') > -1) {
            return true;
        }
        return null;
    }
   
 

    
}
