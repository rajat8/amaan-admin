import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder,FormControl } from '@angular/forms';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { RoleGroupService } from '../rolegroup.service';
import { RoleGroupModel } from '../rolegroup.model';

@Component({
  selector: 'app-createrolegroup',
  templateUrl: './createrolegroup.component.html',
  styleUrls: ['./createrolegroup.component.css']
})
export class CreateRoleGroup implements OnInit {

  createRoleGroupForm: FormGroup;
  editMode = false;
  paramList :any;
  roleGroupModel: RoleGroupModel;
  id : number;
  code : String;
  name: String
  roleGrpParentCode : String;
  permissionCode : String;
  status : String;
  createdOn : Date;
  createdBy : String;
  updatedOn :  Date;
  updatedBy :String;

  constructor(private roleGroupService: RoleGroupService,private formBuilder: FormBuilder, private router: Router, private activatedrouter: ActivatedRoute,
    private route: ActivatedRoute) { }

  createForm() {
    this.createRoleGroupForm = this.formBuilder.group({ 
     id: ['', [Validators.required, Validators.minLength(3)]],
     template: ['', [Validators.required, Validators.minLength(3)]],
     feature: ['', [Validators.required]],
     createdOn: ['', [Validators.required]],
     create: ['', [ ]],
     edit: ['', [ ]],
     view: ['', [ ]],
     approve: ['', [ ]],
     status : ['']
 
    });
  }
  

  ngOnInit() {
   
 
    this.editMode = this.roleGroupService.prepareUserAction(); 
    console.log('--editMode--', this.editMode);   
    

       this.route.params.subscribe((params: Params) => {
       
    
      if(params.id) {
        // this.CreateSystemUserForm = this.formBuilder.group({ 
        //   id: [params.id, [Validators.required, Validators.minLength(3)]],
        //   name: ['', [Validators.required, Validators.minLength(3)]],
        //   status: ['', [Validators.required]],
        //   createdOn: ['', [Validators.required]],
        //   feild1: ['', [Validators.required, Validators.minLength(3)]],
        //   feild2: ['', [Validators.required, Validators.minLength(3)]]
        //  });
        this.roleGroupModel = this.roleGroupService.geSystemUserById(+params.id);
        this.id =  this.roleGroupModel.id;
        this.code = this.roleGroupModel.code;
        this.name = this.roleGroupModel.name;
        this.roleGrpParentCode = this.roleGroupModel.roleGrpParentCode;
        this.permissionCode = this.roleGroupModel.permissionCode;
        this.createdOn = this.roleGroupModel.createdOn;
        this.createdBy = this.roleGroupModel.createdBy;
        this.status = this.roleGroupModel.status;

      }
      this.createForm();
       
    });
  }

  navigateToSytemUserCreation(){
    const firstPath = window.location.pathname.split('/')[1];
    if(this.editMode){
      this.router.navigate( ['../../'], {relativeTo: this.route} );
    }else{
      this.router.navigate( ['../'], {relativeTo: this.route} );
    }
  }

  onReset() {
    this.createRoleGroupForm.reset();
  }
  onCancel(){
    const firstPath = window.location.pathname.split('/')[1];
    if(this.editMode){
      this.router.navigate( ['../../'], {relativeTo: this.route} );
    }else{
      this.router.navigate( ['../'], {relativeTo: this.route} );
    }

  }

  onSubmit() {
    console.log('++submitted+++' + window.location.pathname.split('/')[1]);
    const firstPath = window.location.pathname.split('/')[1];
    if(this.editMode){
      this.router.navigate( ['../../'], {relativeTo: this.route} );
    }else{
      this.router.navigate( ['../'], {relativeTo: this.route} );
    }
  }

  // navgateToStep1() {
  //   document.getElementById('step-1-tab').click();
  // }
  // navgateToStep2() {
  //   document.getElementById('step-2-tab').click();
  // }
  // navgateToStep3() {
  //   document.getElementById('step-3-tab').click();
  // }

  finalSubmit() {
    
  }
}
