import { Component, OnInit } from '@angular/core';
import { RoleGroupService } from './rolegroup.service';
import { RoleGroupModel } from './rolegroup.model';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleGroupModule } from './rolegroup.module';

@Component({
  selector: 'app-rolegroup',
  templateUrl: './rolegroup.component.html',
  styleUrls: ['./rolegroup.component.css']
})
export class RoleGroup implements OnInit {

    private users: RoleGroupModel[];
    constructor(private roleGroupService: RoleGroupService,
      private route: ActivatedRoute,
      private router: Router) { 
    }
  
    ngOnInit() {
        this.users = this.roleGroupService.fetchSystemUserList();
    }

    viewRoleGroup(userid: number) {
 
      this.router.navigate( ['view', userid], {relativeTo: this.route} );
    }

    editRoleGroup(userid: any) {
      this.router.navigate( ['edit', userid], {relativeTo: this.route} );
 
    }
  
  }