
import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { RoleGuard } from '../guards/role.guard';
import { ChangepasswordComponent } from './changepasswordcomponent/changepassword.component';

export const changepasswordRoutes: Routes = [ 
  {
    path : '',
  //  canActivate: [AuthGuard],
    children : [
      {
        path : 'changepassword',
        component : ChangepasswordComponent,
      //  canActivate: [RoleGuard],
        data : {
          expectedRole: '/password/changepassword',  
          title: 'lbl_password_changepassword',
          urls: [
            { title: 'LABEL_HOME', url: '/dashboard/dashboard1' }, 
            { title: 'btn_changepassword', url: '/password/changepassword' },   
           // { title: 'brcr_changepassword' }
        ]   
        }
      },
	    
    ]
  }
];
