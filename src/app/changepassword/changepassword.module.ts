
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../component/component.module';
import { SharedModule } from '../shared-module/shared-module.module';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { changepasswordRoutes } from './changepassword.routing';
import { ChangepasswordComponent } from "./changepasswordcomponent/changepassword.component";
import { ChangepasswordService } from "./changepasswordservice/changepassword.service";
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(changepasswordRoutes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    ComponentsModule,
    SharedModule,
    DataTablesModule,
    NgbModule

  ],
  declarations: [
    ChangepasswordComponent
  ],
   providers:[
    ChangepasswordService
   ]
})
export class ChangepasswordModule { }
