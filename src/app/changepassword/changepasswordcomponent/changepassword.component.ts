import { Component , OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChangePasswordValidator } from './changepassword.validator';
import { Router , ActivatedRoute} from '@angular/router';
import { ChangepasswordrequestModel } from '../model/changepasswordrequest.model';
import { ChangepasswordService } from '../changepasswordservice/changepassword.service';
import { formatDate} from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { API_URLs } from '../../shared/models/constants';
import { ChangepasswordModel } from '../model/changepassword.model';
import { MasterDataRequestModel } from '../../authentication/login/masterdatarequest.model';
import { ConfigService } from '../../shared/services/config.service';
import { MasterDataService } from '../../shared/services/masterdataservice.service';
import { EncryptData } from '../../shared/services/encryptdata.service';
import { LogoutRequestModel } from '../../shared/models/logoutrequest.model';
import { LogoutModel } from '../../shared/models/logout.model';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})

export class ChangepasswordComponent implements OnInit{

  changePasswordFormGroup : FormGroup;
  changepasswordmodel : ChangepasswordModel = new ChangepasswordModel();
  changepasswordrequest : ChangepasswordrequestModel = new ChangepasswordrequestModel();
  error = "";
  changepassworddateandtime : string;
  errorMessage : string;
  languagecode : string;
  //passwordpattern : "/^(?=.[A-Za-z])(?=.\d)[A-Za-z\d]{4,20}/";
  masterdatarequest: MasterDataRequestModel = new MasterDataRequestModel();
  logoutrequest : LogoutRequestModel = new LogoutRequestModel();
  logoutdata : LogoutModel = new LogoutModel();
  submitted = false;
  loginuserId : string;

  constructor(private formBuilder: FormBuilder , private router: Router , private changepasswordauthenticationservice : ChangepasswordService,
  private translateservice : TranslateService , private activatedrouter : ActivatedRoute, private apiurls: API_URLs,private configService:ConfigService,private masterdataservice:MasterDataService , private encryptionService : EncryptData) {
  }
  ngOnInit(){

    this.activatedrouter.queryParams.subscribe(params =>{
      console.log("language : "+params.language);
      this.languagecode = params.language;
      this.translateservice.use(params.language);
      });

    this.changePasswordFormGroup = this.formBuilder.group({
      password : ['',[Validators.required , Validators.pattern('((?=.*\\d){2}(?=(.*[A-Z]){2})(?=.*[!@#$%^*:?()\\-`.+,/\]).{10,15})')]],
      newPassword : ['',[Validators.required , Validators.pattern('((?=.*\\d){2}(?=(.*[A-Z]){2})(?=.*[!@#$%^*:?()\\-`.+,/\]).{10,15})')]],
      confirmPassword : ['',Validators.required],
     },{
      validator: ChangePasswordValidator.validate.bind(this)
    })
    this.loginuserId = sessionStorage.getItem("currentuser").toString();
  }


  get f() { return this.changePasswordFormGroup.controls; }
  

  onSubmit() {
      
      this.submitted = true;
      this.changepassworddateandtime = formatDate(new Date() ,  "yyyy-mm-dd HH:mm:ss" , "en");
      this.changepasswordmodel.password = this.encryptionService.encryptData(this.changePasswordFormGroup.value.password).toUpperCase();;
      this.changepasswordmodel.newPassword = this.encryptionService.encryptData(this.changePasswordFormGroup.value.newPassword).toUpperCase();;
      this.changepasswordmodel.confirmPassword = this.encryptionService.encryptData(this.changePasswordFormGroup.value.confirmPassword).toUpperCase();;
      this.changepasswordmodel.loginId = sessionStorage.getItem("currentuser");
      this.changepasswordmodel.userId = sessionStorage.getItem("userId");
      this.changepasswordmodel.channel = "IWP";
      this.changepasswordmodel.comments = "Change Password Request";
      this.changepasswordmodel.requestcts = this.changepassworddateandtime;
      this.changepasswordrequest.action = "CHANGEPASSWORD";
      this.changepasswordrequest.request = this.changepasswordmodel;

      console.log("clicked");
      
      if(this.changePasswordFormGroup.invalid ) {
          return;
      }
      
      console.log("ChangePassword request : "+JSON.stringify( this.changepasswordrequest));
      
    this.changepasswordauthenticationservice.changePassword(this.apiurls.URL_CHANGEPASSWORD, this.changepasswordrequest)
    .then(
      data => {
        console.log("data : " + JSON.stringify(data));
          
        if (data == null) {
          this.translateservice.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
        } else if (data["response"]["resultCode"] == this.apiurls.RESULT_CODE) {
          
          console.log("ChangePassword response : "+JSON.stringify(data));
          if(data.response.resultCode == this.apiurls.RESULT_CODE){
            console.log("languagecode : "+this.languagecode);
            this.masterAfterChangePassword();            
          }else{
            this.errorMessage = data.response.resultCodeDesc;
          }

        } else {
          this.errorMessage = "Failed : Error Code : " + data["response"]["resultCode"] + " : Error Desc : " + data["response"]["resultCodeDesc"];
        }
      }
    );
  }

  masterAfterChangePassword(){
    this.masterdatarequest.action = "MASTERDATA";
    this.masterdatarequest.request = {};
    console.log("MASTERDATA Request : " + JSON.stringify(this.masterdatarequest));
   // console.log("SavedServerPath: " + this.configService.getURL());

    this.masterdataservice.getData(this.apiurls.URL_MASTERDATA, this.masterdatarequest)
      .then(data => {
 
        if (data == null) {
          this.translateservice.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
        } else if (data == "error") {
          this.translateservice.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
        } else if (data["response"]["resultCode"] == this.apiurls.RESULT_CODE) {
            console.log("Master data successfully loaded");
            this.router.navigate(["/dashboard/dashboard1"],{queryParams:{language:this.languagecode}});  
       //   this.router.navigate(["/authentication/login"],{queryParams:{language:this.languagecode}});     
            this.logOut();
         } else {
          this.errorMessage = "Failed Error Code : " + data["response"]["resultCode"] + " : Error Desc : " + data["response"]["resultCodeDesc"];
        }
      });
  }
  logOut(){
    // this.logoutdata.loginId = sessionStorage.getItem('loginId');
    this.logoutdata.loginId = sessionStorage.getItem("currentuser");
    this.logoutdata.channel = "IWP";
    this.logoutdata.comments = "User Logout request";
    this.logoutdata.requestcts = formatDate(new Date(),"yyyy-MM-dd HH:mm:ss.SSS", "en");
    this.logoutdata.userId = sessionStorage.getItem("userId");
    this.logoutrequest.action = "LOGOUT";
    this.logoutrequest.request = this.logoutdata;
    console.log("LOGOUT request : "+ JSON.stringify(this.logoutrequest));       
  //  this.loginauthenticationservice.logOut(this.apiurls.URL_LOGOUT , this.logoutrequest)
    this.changepasswordauthenticationservice.changePassword(this.apiurls.URL_LOGOUT,this.logoutrequest)
    .then(
      data => {
        console.log("data : " + JSON.stringify(data));
        if (data == null) {
          this.translateservice.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
        } else if (data["response"]["resultCode"] == this.apiurls.RESULT_CODE) {          
          this.router.navigate(["/authentication/login"],{queryParams: {refresh: new Date()}});    
       //   setTimeout('window.location.reload()',500);
        } else {                    
          this.translateservice.get('err_logout').subscribe((text: string) => { this.errorMessage = text; });
         // alert(this.errorMessage);          
        }
      }
    );
  

}
backToDashBoard() {
  this.router.navigate(["/dashboard/dashboard1"], { queryParams: { language: this.languagecode } });
}
}
