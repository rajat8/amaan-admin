import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder,FormControl } from '@angular/forms';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { RolePermissionService } from '../rolepermission.service';
import { RolePermissionModel } from '../rolepermission.model';

@Component({
  selector: 'app-createrolepermission',
  templateUrl: './createrolepermission.component.html',
  styleUrls: ['./createrolepermission.component.css']
})
export class CreateRolePermission implements OnInit {

  createRolePermissionForm: FormGroup;
  editMode = false;
  paramList :any;
  rolePermissionModel: RolePermissionModel;
   id: number;
   createdOn: Date;
   template :String;
   feature :String;
   view : Boolean;
   edit :Boolean;
   create :Boolean;
   approve :Boolean;
   status = '';
  constructor(private systemUserConfigurationService: RolePermissionService,private formBuilder: FormBuilder, private router: Router, private activatedrouter: ActivatedRoute,
    private route: ActivatedRoute) { }

  createForm() {
    this.createRolePermissionForm = this.formBuilder.group({ 
     id: ['', [Validators.required, Validators.minLength(3)]],
     template: ['', [Validators.required, Validators.minLength(3)]],
     feature: ['', [Validators.required]],
     createdOn: ['', [Validators.required]],
     create: ['', [ ]],
     edit: ['', [ ]],
     view: ['', [ ]],
     approve: ['', [ ]],
     status : ['']
 
    });
  }
  

  ngOnInit() {
   
 
    this.editMode = this.systemUserConfigurationService.prepareUserAction(); 
    console.log('--editMode--', this.editMode);   
    

       this.route.params.subscribe((params: Params) => {
       
    
      if(params.id) {
        // this.CreateSystemUserForm = this.formBuilder.group({ 
        //   id: [params.id, [Validators.required, Validators.minLength(3)]],
        //   name: ['', [Validators.required, Validators.minLength(3)]],
        //   status: ['', [Validators.required]],
        //   createdOn: ['', [Validators.required]],
        //   feild1: ['', [Validators.required, Validators.minLength(3)]],
        //   feild2: ['', [Validators.required, Validators.minLength(3)]]
        //  });
        this.rolePermissionModel = this.systemUserConfigurationService.geSystemUserById(+params.id);
        this.id =  this.rolePermissionModel.id;
        this.template = this.rolePermissionModel.template;
        this.feature = this.rolePermissionModel.feature;
        this.edit = this.rolePermissionModel.edit;
        this.approve = this.rolePermissionModel.approve;
        this.view = this.rolePermissionModel.view;
        this.create = this.rolePermissionModel.create;
        this.createdOn = this.rolePermissionModel.createdOn;
        this.status = this.rolePermissionModel.status;
        
        
     
      }
      this.createForm();
       
    });
  }

  navigateToSytemUserCreation(){
    const firstPath = window.location.pathname.split('/')[1];
    if(this.editMode){
      this.router.navigate( ['../../'], {relativeTo: this.route} );
    }else{
      this.router.navigate( ['../'], {relativeTo: this.route} );
    }
  }

  onReset() {
    this.createRolePermissionForm.reset();
  }
  onCancel(){
    const firstPath = window.location.pathname.split('/')[1];
    if(this.editMode){
      this.router.navigate( ['../../'], {relativeTo: this.route} );
    }else{
      this.router.navigate( ['../'], {relativeTo: this.route} );
    }

  }

  onSubmit() {
    console.log('++submitted+++' + window.location.pathname.split('/')[1]);
    const firstPath = window.location.pathname.split('/')[1];
    if(this.editMode){
      this.router.navigate( ['../../'], {relativeTo: this.route} );
    }else{
      this.router.navigate( ['../'], {relativeTo: this.route} );
    }
  }

  // navgateToStep1() {
  //   document.getElementById('step-1-tab').click();
  // }
  // navgateToStep2() {
  //   document.getElementById('step-2-tab').click();
  // }
  // navgateToStep3() {
  //   document.getElementById('step-3-tab').click();
  // }

  finalSubmit() {
    
  }
}
