import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
 
import { RolePermission } from './rolepermission.component';
import { CreateRolePermission } from './createrolepermission/createrolepermission.component';
import { ViewRolePermission } from './viewrolepermission/viewrolepermission.component';
const routes: Routes = [
    { path: '', component: RolePermission, pathMatch: "full" },
    { path: 'add', component: CreateRolePermission },
    { path: 'view/:id', component: ViewRolePermission },
    { path: 'edit/:id', component: CreateRolePermission }
  ];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class RolePermissionRoutingModule{}