export interface RolePermissionModel {
    id: number,
    template: String,
    feature: String
    createdOn: Date,
    create: Boolean;
    view: Boolean;
    edit: Boolean;
    approve: Boolean;
    status : string
 
 
   
}