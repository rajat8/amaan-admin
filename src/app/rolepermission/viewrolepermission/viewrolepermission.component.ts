import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder,FormControl } from '@angular/forms';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { RolePermissionService } from '../rolepermission.service';
import { RolePermissionModel } from '../rolepermission.model';

@Component({
  selector: 'app-viewrolepermission',
  templateUrl: './viewrolepermission.component.html',
  styleUrls: ['./viewrolepermission.component.css']
})
export class ViewRolePermission implements OnInit {

  
  editMode = false;
  paramList :any;
  createSystemUserModel: RolePermissionModel;
  id: number;
  createdOn: Date;
  template :String;
  feature :String;
  view : Boolean;
  edit :Boolean;
  create :Boolean;
  approve :Boolean;
  status = '';
  constructor(private systemUserConfigurationService: RolePermissionService,private formBuilder: FormBuilder, private router: Router, private activatedrouter: ActivatedRoute,
    private route: ActivatedRoute) { }

   
  

  ngOnInit() {

      this.route.params.subscribe((params: Params) => {
       
    
      if(params.id) {
      console.log("params" + params.id);
        this.createSystemUserModel = this.systemUserConfigurationService.geSystemUserById(+params.id);
        this.id =  this.createSystemUserModel.id;
        this.template = this.createSystemUserModel.template;
        this.feature = this.createSystemUserModel.feature;
        this.edit = this.createSystemUserModel.edit;
        this.approve = this.createSystemUserModel.approve;
        this.view = this.createSystemUserModel.view;
        this.create = this.createSystemUserModel.create;
        this.createdOn = this.createSystemUserModel.createdOn;
        this.status = this.createSystemUserModel.status;
        
        
     
      }
     
       
    });
  }

  navigateToSytemUserCreation(){
    
      this.router.navigate( ['../../'], {relativeTo: this.route} );
    
  }
   

  onCancel() {
    
  
      this.router.navigate( ['../../'], {relativeTo: this.route} );
     
  }
 
}
