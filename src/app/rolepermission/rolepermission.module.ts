import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateRolePermission } from './createrolepermission/createrolepermission.component';
import { RolePermissionRoutingModule } from './rolepermission-routing.module';
import { RolePermissionService } from './rolepermission.service';
import { RolePermission} from './rolepermission.component';
import { ViewRolePermission } from './viewrolepermission/viewrolepermission.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [ RolePermission,CreateRolePermission,ViewRolePermission],
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      RolePermissionRoutingModule,
      
      
    ],
    providers: [
      RolePermissionService
    ]
  })
export class RolePermissionModule{}