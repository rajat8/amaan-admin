import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RolePermissionModel } from './rolepermission.model';
import { Router } from '@angular/router';

@Injectable()
export class RolePermissionService {

    constructor(private router: Router) { }

    private rolePermissionModel: RolePermissionModel[];
 

    public fetchSystemUserList(): RolePermissionModel[] {
        const systemUsers: RolePermissionModel[] = [

       
            {
                id: 101,
                template: '10001',
                feature: 'WALLETOWNER',
                createdOn: new Date(),
                view: true,
                create : true,
                edit:true,
                approve:true,
                status : 'Inactive'
                
            },
            {
                id: 102,
                template: '10002',
                feature: 'WALLETOWNER',
                createdOn: new Date(),
                view: true,
                create : true,
                edit: true,
                approve:false,
                status : 'Inactive'
                
            },
            {
                id: 103,
                template: '10003',
                feature: 'WALLETOWNER',
                createdOn: new Date(),
                view: true,
                create : false,
                edit:true,
                approve:false,
                status : 'Active'
                
            },
            {
                id: 104,
                template: '10004',
                feature: 'WALLETOWNER',
                createdOn: new Date(),
                view: false,
                create : true,
                edit:false,
                approve:false,
                status : 'Active'
                
            },
            
        ];

        this.rolePermissionModel = systemUsers;
        return systemUsers;
    }

    public geSystemUserById(systemUserId: number): RolePermissionModel {
        for (let i = 0; i < this.rolePermissionModel.length; i++) {
            if (this.rolePermissionModel[i].id === systemUserId) {
                return this.rolePermissionModel[i];
            }
        }
       // return null;
    }

    public getCurrentURL() {
        return this.router.url;
    }

    public prepareUserAction(): boolean {
        const currentURL = this.getCurrentURL();
        if (currentURL.indexOf('/add') > -1) {
            return false;
        } else if (currentURL.indexOf('/edit') > -1) {
            return true;
        }
        return null;
    }
   
 

    
}
