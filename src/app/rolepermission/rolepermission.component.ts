import { Component, OnInit } from '@angular/core';
import { RolePermissionService } from './rolepermission.service';
import { RolePermissionModel } from './rolepermission.model';
import { ActivatedRoute, Router } from '@angular/router';
import { RolePermissionModule } from './rolepermission.module';

@Component({
  selector: 'app-rolepermission',
  templateUrl: './rolepermission.component.html',
  styleUrls: ['./rolepermission.component.css']
})
export class RolePermission implements OnInit {

    private users: RolePermissionModel[];
    constructor(private rolePermissionService: RolePermissionService,
      private route: ActivatedRoute,
      private router: Router) { 
    }
  
    ngOnInit() {
        this.users = this.rolePermissionService.fetchSystemUserList();
    }

    viewRolePermission(userid: number) {
      // const systemUser: SystemUserConfigurationModel = this.systemUserConfigurationService.geSystemUserById(systemUserId);
      // console.log(systemUser);
      this.router.navigate( ['view', userid], {relativeTo: this.route} );
    }

    editRolePermission(userid: any) {
      this.router.navigate( ['edit', userid], {relativeTo: this.route} );
   //   this.router.navigate(["/artwork/modifyartwork"], { queryParams: value, skipLocationChange: true });

    }
  
  }