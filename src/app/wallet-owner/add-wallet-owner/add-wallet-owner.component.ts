import { Component, OnInit, Inject } from '@angular/core';
import { WalletOwnerService } from '../wallet-owner.service';
import { FormGroup, FormArray } from '@angular/forms';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { WalletOwnerModel } from '../wallet-owner.model';
import { DOCUMENT } from '@angular/common';
import { AddressService } from './address/address.service';

@Component({
  selector: 'app-add-wallet-owner',
  templateUrl: './add-wallet-owner.component.html',
  styleUrls: ['./add-wallet-owner.component.css']
})
export class AddWalletOwnerComponent implements OnInit {

  walletOwnerForm: FormGroup;
  basicInformationForm: FormGroup;
  editMode = false;
  walletOwner: WalletOwnerModel;
  selectedChannels: string[];
  selectedWallets: string[];
  channels: any[];
  wallets: any[];
  categories: any[];
  businessTypes: any[];
  contractTypes: any[];
  states: any[];
  groups: any[];
  formAddresses = new FormArray([]);
  allAddress: FormGroup; 

  constructor(private walletOwnerService: WalletOwnerService,
    private route: ActivatedRoute,
    private addressService: AddressService,
    @Inject(DOCUMENT) document) {    }

  ngOnInit() {
    this.editMode = this.walletOwnerService.prepareUserAction();
    this.addressService.createAllAddress();
    this.allAddress = this.addressService.allAddress;
  }

  navigateToWalletOwners() {
  }

  navgateToStep1() {
    document.getElementById('step-1-tab').click();
  }

  navgateToStep2() {
    document.getElementById('step-2-tab').click();
  }

  navgateToStep3() {
    console.log('--navgateToStep3--');
    console.log(this.addressService.allAddress.value);

    // document.getElementById('step-3-tab').click();
  }

  get getAddressesControl() {
    return this.addressService.getAddressesControl;
  }

}
