import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BasicInfoService } from './basic-info.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.css']
})
export class BasicInfoComponent implements OnInit {

  basicInformationForm: FormGroup;
  selectedChannels: string[];
  selectedWallets: string[];
  channels: any[];
  wallets: any[];
  categories: any[];
  businessTypes: any[];
  contractTypes: any[];
  states: any[];
  groups: any[];
  channelsValidations = false;
  walletsValidations = false;

  constructor(private basicInfoService: BasicInfoService,
    @Inject(DOCUMENT) document) { }

  ngOnInit() {
    this.categories = this.basicInfoService.categories;
    this.contractTypes = this.basicInfoService.contractTypes;
    this.businessTypes = this.basicInfoService.businessTypes;
    this.states = this.basicInfoService.states;
    this.channels = this.basicInfoService.channels;
    this.wallets = this.basicInfoService.wallets;
    this.groups = this.basicInfoService.groups;

    this.basicInformationForm = this.basicInfoService.createBasicInformationFormGroup();
  }


  /**
   * methos is to get selected items in multiselect box
   * @param items : selected items ID
   */
  recieveItem(items: any) {
    if (items.type === 'channel') {
      this.selectedChannels = items.selected;
    } else if (items.type === 'wallet') {
      this.selectedWallets = items.selected;
    }
  }

  /**
   * basicInformation form submission
   */
  onSubmitForm() {

    this.validateChannels();
    this.validateWallets();

    if (this.basicInformationForm.valid && !this.channelsValidations && !this.walletsValidations) {
      // after all validation and checks
      this.basicInfoService.setPatchValue(this.basicInformationForm, this.selectedChannels, this.selectedWallets);
      this.basicInfoService.prepareCreateWalletOwnerRequest(this.basicInformationForm.value);

      // create wallet api call; after successfully wallet owner creation
      document.getElementById('step-2-tab').click();
    } else {
      console.log('--INVALID--');
      
    }

  }

  validateChannels() {
    if(this.selectedChannels && this.selectedChannels.length) {
      this.channelsValidations = false;
    } else{
      this.channelsValidations = true;
    }
  }

  validateWallets() {
    if(this.selectedWallets && this.selectedWallets.length) {
      this.walletsValidations = false;
    } else{
      this.walletsValidations = true;
    }
  }

}
