import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common'
import { CreateWalletOwnerRequest } from '../create-wallet-owner-request.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable()
export class BasicInfoService {

  constructor(private datePipe: DatePipe) { }

  /**
   * This method is to create basicInformationForm
   */
  public createBasicInformationFormGroup(): FormGroup {

    const basicInformationForm = new FormGroup({
      category: new FormControl('', [Validators.required]),
      contractType: new FormControl('', [Validators.required]),
      ownerName: new FormControl('', [Validators.required]),
      aliasName: new FormControl('', [Validators.required]),
      hrId: new FormControl('', [Validators.required]),
      nationalId: new FormControl('', [Validators.required]),
      businessName: new FormControl('', [Validators.required]),
      businessType: new FormControl('', [Validators.required]),
      state: new FormControl('', [Validators.required]),
      stage: new FormControl('', [Validators.required]),
      lineOfBusiness: new FormControl('', [Validators.required]),
      mobile: new FormControl('', [Validators.required]),
      group: new FormControl('', [Validators.required]),
      channel: new FormControl(),
      wallet: new FormControl()
    });

    return basicInformationForm;
    // return this.autofilledBasicForm();
  }

  /**
   * This method is to auto fill basic form; just to development; to get rid of fill each item
   */
  autofilledBasicForm(): FormGroup {
    const basicInformationForm = new FormGroup({
      category: new FormControl('10001', [Validators.required]),
      contractType: new FormControl('50002', [Validators.required]),
      ownerName: new FormControl('Ankit Tanwar', [Validators.required]),
      aliasName: new FormControl('ankit', [Validators.required]),
      hrId: new FormControl('HR1234', [Validators.required]),
      nationalId: new FormControl('Aadhar', [Validators.required]),
      businessName: new FormControl('Software firm', [Validators.required]),
      businessType: new FormControl('40004', [Validators.required]),
      state: new FormControl('60003', [Validators.required]),
      stage: new FormControl('Development', [Validators.required]),
      lineOfBusiness: new FormControl('Mobile Shop', [Validators.required]),
      mobile: new FormControl('9650041902', [Validators.required]),
      group: new FormControl('70004', [Validators.required]),
      channel: new FormControl(),
      wallet: new FormControl()
    });
    return basicInformationForm;
  }

  get categories(): any[] {
    return [
      {
        code: '10001',
        name: 'Category 1'
      },
      {
        code: '10002',
        name: 'Category 2'
      },
      {
        code: '10003',
        name: 'Category 3'
      },
      {
        code: '10004',
        name: 'Category 4'
      }
    ];
  }

  get channels(): any[] {
    return [
      {
        id: '20001',
        text: 'Channel 1'
      },
      {
        id: '20002',
        text: 'Channel 2'
      },
      {
        id: '20003',
        text: 'Channel 3'
      },
      {
        id: '20004',
        text: 'Channel 4'
      }
    ];
  }

  get wallets(): any[] {
    return [
      {
        id: '30001',
        text: 'Wallet 1'
      },
      {
        id: '30002',
        text: 'Wallet 2'
      },
      {
        id: '30003',
        text: 'Wallet 3'
      },
      {
        id: '30004',
        text: 'Wallet 4'
      }
    ];
  }

  get businessTypes(): any[] {
    return [
      {
        code: '40001',
        name: 'BusinessType 1'
      },
      {
        code: '40002',
        name: 'BusinessType 2'
      },
      {
        code: '40003',
        name: 'BusinessType 3'
      },
      {
        code: '40004',
        name: 'BusinessType 4'
      }
    ];
  }

  get contractTypes(): any[] {
    return [
      {
        code: '50001',
        name: 'ContractType 1'
      },
      {
        code: '50002',
        name: 'ContractType 2'
      },
      {
        code: '50003',
        name: 'ContractType 3'
      },
      {
        code: '50004',
        name: 'ContractType 4'
      }
    ];
  }

  get states(): any[] {
    return [
      {
        code: '60001',
        name: 'State 1'
      },
      {
        code: '60002',
        name: 'State 2'
      },
      {
        code: '60003',
        name: 'State 3'
      },
      {
        code: '60004',
        name: 'State 4'
      }
    ];
  }

  get groups(): any[] {
    return [
      {
        code: '70001',
        name: 'Group 1'
      },
      {
        code: '70002',
        name: 'Group 2'
      },
      {
        code: '70003',
        name: 'Group 3'
      },
      {
        code: '70004',
        name: 'Group 4'
      }
    ];
  }

  /**
   * prepare owner wallets in desired format
   * @param walletIds : selectded wallets
   */
  private prepareOwnersWallet(walletIds: string[]) {
    let walletOwnerWallet = [];
    walletIds.forEach(function (walletId) {
      walletOwnerWallet.push(
        {
          wltTypeCode: walletId
        });
    });
    return walletOwnerWallet;
  }

  /**
   * prepare owner channels in desired format
   * @param channelIds : selected channels
   */
  private prepareOwnersChannel(channelIds: string[]) {
    let walletOwnerChannel = [];
    channelIds.forEach(function (channelId) {
      walletOwnerChannel.push(
        {
          chlTypeCode: channelId
        });
    });
    return walletOwnerChannel;
  }

  /**
   * setting the multiselect value in form
   * @param basicInformationForm : basicInfo form
   * @param selectedChannels : selectded channel ids
   * @param selectedWallets : selected wallet ids
   */
  setPatchValue(basicInformationForm: FormGroup, selectedChannels: string[], selectedWallets: string[]) {
    basicInformationForm.patchValue({
      channel: selectedChannels,
      wallet: selectedWallets
    });
  }

  /**
   * method is to prepare CreateWalletOwnerRequest
   * @param data : user provided details
   */
  public prepareCreateWalletOwnerRequest(data: any) {
    const createWalletOwnerRequest: CreateWalletOwnerRequest = {
      walletownerCatCode: data.category,
      ownerName: data.ownerName,
      aliasName: data.aliasName,
      hrid: data.hrId,
      nationalId: data.nationalId,
      mobileNumber: data.mobile,
      businessName: data.businessName,
      businessType: data.businessType,
      lineOfBusiness: data.lineOfBusiness,
      registrationDate: this.datePipe.transform(new Date(), 'dd-MM-yyyy'),
      contractTypeCode: data.contractType,
      state: data.state,
      stage: data.stage,
      groupCode: data.group,
      walletOwnerWallet: this.prepareOwnersWallet(data.wallet),
      walletOwnerChannel: this.prepareOwnersChannel(data.channel)
    };
    console.log('--createWalletOwnerRequest--', createWalletOwnerRequest);
  }
}
