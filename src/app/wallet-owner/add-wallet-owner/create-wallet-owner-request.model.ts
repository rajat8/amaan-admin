export interface CreateWalletOwnerRequest {
    walletownerCatCode: string;
    ownerName: string;
    aliasName: string;
    hrid: string;
    nationalId: string;
    mobileNumber: string;
    businessName: string;
    businessType: string;
    lineOfBusiness: string;
    registrationDate: string,
    contractTypeCode: string;
    state: string;
    stage: string;
    groupCode: string;
    walletOwnerWallet: {
        wltTypeCode: string;
    }[];
    walletOwnerChannel: {
        chlTypeCode: string;
    }[];
}