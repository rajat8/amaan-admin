import { Injectable } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

@Injectable()
export class AddressService {

  formAddresses = new FormArray([]);
  allAddress: FormGroup; 
  savedAddress: any[] = [];
  constructor() { }

  get addressTypes(): any[] {
    return [
      {
        code: '10001',
        name: 'Address Type 1'
      },
      {
        code: '10002',
        name: 'Address Type 2'
      },
      {
        code: '10003',
        name: 'Address Type 3'
      },
      {
        code: '10004',
        name: 'Address Type 4'
      }
    ];
  }

  get governorates(): any[] {
    return [
      {
        code: '20001',
        name: 'Governorate 1'
      },
      {
        code: '20002',
        name: 'Governorate 2'
      },
      {
        code: '20003',
        name: 'Governorate 3'
      },
      {
        code: '20004',
        name: 'Governorate 4'
      }
    ];
  }

  get regions(): any[] {
    return [
      {
        code: '30001',
        name: 'Region 1'
      },
      {
        code: '30002',
        name: 'Region 2'
      },
      {
        code: '30003',
        name: 'Region 3'
      },
      {
        code: '30004',
        name: 'Region 4'
      }
    ];
  }

  get territories(): any[] {
    return [
      {
        code: '40001',
        name: 'Territory 1'
      },
      {
        code: '40002',
        name: 'Territory 2'
      },
      {
        code: '40003',
        name: 'Territory 3'
      },
      {
        code: '40004',
        name: 'Territory 4'
      }
    ];
  }

  get regionals(): any[] {
    return [
      {
        code: '50001',
        name: 'Regional 1'
      },
      {
        code: '50002',
        name: 'Regional 2'
      },
      {
        code: '50003',
        name: 'Regional 3'
      },
      {
        code: '50004',
        name: 'Regional 4'
      }
    ];
  }

  createAddressFormGroup(): FormGroup { 
    const addressForm = new FormGroup({
      addressLine1: new FormControl('', [Validators.required]),
      addressLine2: new FormControl('', [Validators.required]),
      region: new FormControl('', [Validators.required]),
      territory: new FormControl('', [Validators.required]),
      location: new FormControl('', [Validators.required]),
      addressType: new FormControl('', [Validators.required]),
      regional: new FormControl('', [Validators.required]),
      governorate: new FormControl('', [Validators.required])
    });
    return addressForm;
  }

  createAllAddress() {
    this.allAddress = new FormGroup({
      adresses: this.formAddresses
    });
  }

  get getAddressesControl() {
    return (this.allAddress.get('adresses') as FormArray).controls;    // Typecasting to FormArray
  }

  onAddAddressForm() {
    this.formAddresses.push(this.createAddressFormGroup());
    this.savedAddress.push(this.createAddressFormGroup().value);
  }

  onRemoveAddressForm(index: number) {
    this.formAddresses.removeAt(index);
  }

}
