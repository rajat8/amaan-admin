import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AddressService } from './address.service';
import { Observable } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  addressForm: FormGroup;
  addressTypes: any[];
  governorates: any[];
  regions: any[];
  territories: any[];
  regionals: any[];

  @Input('formIndex')
  formIndex: number;

  constructor(private addressService: AddressService,
    @Inject(DOCUMENT) document) { }

  ngOnInit() {

    console.log('--formIndex--', this.formIndex);

    this.addressForm = this.addressService.createAddressFormGroup();

    this.addressTypes = this.addressService.addressTypes;
    this.governorates = this.addressService.governorates;
    this.regions = this.addressService.regions;
    this.territories = this.addressService.territories;
    this.regionals = this.addressService.regionals;
  }

  onRemoveAddressForm() {
    console.log('--onRemoveAddressForm--', this.formIndex);
    this.addressService.onRemoveAddressForm(this.formIndex);
    this.addressService.savedAddress.splice(this.formIndex, 1);
  }

  onEditAddressForm() {
    console.log('--onEditAddressForm--', this.formIndex);
    // updating the address(default: null) at formIndex
    this.addressService.savedAddress.splice(this.formIndex, 1, this.addressService.createAddressFormGroup().value);
    this.addressForm.enable();
  }

  submitAddress() {
    document.getElementById('addressform_' + this.formIndex).click();

    console.log('--form valid--', this.addressForm.valid);
    console.log('--line1 valid--', this.addressForm.get('addressLine1').valid, this.addressForm.get('addressLine1'));

    if (this.addressForm.valid) {
      console.log('--form is valid--');
      console.log(this.addressForm.value);
      
      // updating the address(actual: user enterd) at formIndex
      this.addressService.savedAddress.splice(this.formIndex, 1, this.addressForm.value);
      this.addressForm.disable();

      console.log('--size--', this.addressService.savedAddress);
    } else {
      console.log('--form is invalid--');
    }
  }

}
