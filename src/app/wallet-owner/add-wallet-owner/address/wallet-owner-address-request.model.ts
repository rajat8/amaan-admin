export interface WalletOwnerAddressRequest {
    walletOwnerCode: string;
    address: {
        addTypeCode: string;
        regionalArea: string;
        governorateCode: string;
        regionCode: string;
        territoryCode: string;
        addressLine1: string;
        addressLine2: string;
        location: string;
    }[];
}