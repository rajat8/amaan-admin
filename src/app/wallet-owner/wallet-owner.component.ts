import { Component, OnInit } from '@angular/core';
import { WalletOwnerService } from './wallet-owner.service';
import { WalletOwnerModel } from './wallet-owner.model';
import { ActivatedRoute, Router } from '@angular/router';
import { WalletOwnerModule } from './wallet-owner.module';

@Component({
  selector: 'app-wallet-owner',
  templateUrl: './wallet-owner.component.html',
  styleUrls: ['./wallet-owner.component.css']
})
export class WalletOwnerComponent implements OnInit {

    private walletOwners: WalletOwnerModel[];
    constructor(private walletOwnerService: WalletOwnerService,
      private route: ActivatedRoute,
      private router: Router) { 
    }
  
    ngOnInit() {
        this.walletOwners = this.walletOwnerService.fetchWalletOwnerList();
    }

    viewWalletOwner(walletOwnerId: number) {
      const walletOwner: WalletOwnerModule = this.walletOwnerService.getWalletOwnerById(walletOwnerId);
      console.log(walletOwner);
    }

    editWalletOwner(walletOwnerId: number) {
      this.router.navigate( ['edit', walletOwnerId], {relativeTo: this.route} );
    }
  
  }