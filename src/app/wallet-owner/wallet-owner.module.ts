import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { WalletOwnerComponent } from './wallet-owner.component';
import { WalletOwnerRoutingModule } from './wallet-owner-routing.module';
import { WalletOwnerService } from './wallet-owner.service';
import { AddWalletOwnerComponent } from './add-wallet-owner/add-wallet-owner.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressComponent } from './add-wallet-owner/address/address.component';
import { AddWalletOwnerService } from './add-wallet-owner/add-wallet-owner-service';
import { NgSelect2Module } from 'ng-select2';
import { AddressService } from './add-wallet-owner/address/address.service';
import { BasicInfoService } from './add-wallet-owner/basic-info/basic-info.service';
import { BasicInfoComponent } from './add-wallet-owner/basic-info/basic-info.component';
import { WalletOwnerProxyService } from './wallet-owner-proxy.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared-module/shared-module.module';

@NgModule({
    declarations: [WalletOwnerComponent, AddWalletOwnerComponent, AddressComponent, BasicInfoComponent],
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      NgSelect2Module,
      SharedModule,
      WalletOwnerRoutingModule
    ],
    providers: [
        WalletOwnerService,
        AddWalletOwnerService,
        BasicInfoService,
        AddressService,
        HttpClientModule,
        WalletOwnerProxyService,
        DatePipe
    ]
  })
export class WalletOwnerModule{}