import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { WalletOwnerComponent } from './wallet-owner.component';
import { AddWalletOwnerComponent } from './add-wallet-owner/add-wallet-owner.component';

const routes: Routes = [
    { path: '', component: WalletOwnerComponent, pathMatch: "full" },
    { path: 'add', component: AddWalletOwnerComponent },
    { path: 'view/:id', component: AddWalletOwnerComponent },
    { path: 'edit/:id', component: AddWalletOwnerComponent }
  ];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class WalletOwnerRoutingModule{}