import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CreateWalletOwnerRequest } from './add-wallet-owner/create-wallet-owner-request.model';

@Injectable()
export class WalletOwnerProxyService {

  constructor(private http: HttpClient) { }

  CREATE_WALLET_OWNER_URL = '';

  public createWalletOwner(createWalletOwnerRequest: CreateWalletOwnerRequest) {
    this.http.post(this.CREATE_WALLET_OWNER_URL, createWalletOwnerRequest).subscribe(response => {
      console.log('--response of createWalletOwner--', response);
    }, error => {
      console.log('--error in createWalletOwner--', error);
    });
  }

}
