export interface WalletOwnerModel {
    id: number;
    name: string;
    status: string;
    createdOn: Date;
    userMobile: number;
    userType: string;
    feild1: string;
    type: string;
}