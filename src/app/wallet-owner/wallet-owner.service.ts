import { WalletOwnerModel } from './wallet-owner.model';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class WalletOwnerService {

    constructor(private router: Router) { }

    private walletOwners: WalletOwnerModel[];

    public fetchWalletOwnerList(): WalletOwnerModel[] {
        const walletOwners: WalletOwnerModel[] = [
            {
                id: 100801,
                name: 'name 1',
                status: 'active',
                createdOn: new Date(),
                userMobile: 9872567899,
                userType: 'subscriber',
                feild1: 'value 1',
                type: 'abc'
            },
            {
                id: 100802,
                name: 'name 2',
                status: 'inactive',
                createdOn: new Date(),
                userMobile: 9872567890,
                userType: 'subscriber',
                feild1: 'value 1',
                type: 'abc'
            },
            {
                id: 100803,
                name: 'name 2',
                status: 'inactive',
                createdOn: new Date(),
                userMobile: 9872567890,
                userType: 'subscriber',
                feild1: 'value 1',
                type: 'abc'
            },
            {
                id: 100804,
                name: 'name 2',
                status: 'inactive',
                createdOn: new Date(),
                userMobile: 9872567890,
                userType: 'subscriber',
                feild1: 'value 1',
                type: 'abc'
            },
            {
                id: 100805,
                name: 'name 2',
                status: 'inactive',
                createdOn: new Date(),
                userMobile: 9872567890,
                userType: 'subscriber',
                feild1: 'value 1',
                type: 'abc'
            },
            {
                id: 100806,
                name: 'name 2',
                status: 'inactive',
                createdOn: new Date(),
                userMobile: 9872567890,
                userType: 'subscriber',
                feild1: 'value 1',
                type: 'abc'
            },
            {
                id: 100807,
                name: 'name 2',
                status: 'inactive',
                createdOn: new Date(),
                userMobile: 9872567890,
                userType: 'subscriber',
                feild1: 'value 1',
                type: 'abc'
            }
        ];

        this.walletOwners = walletOwners;
        return walletOwners;
    }

    public getWalletOwnerById(walletOwnerId: number): WalletOwnerModel {
        for (let i = 0; i < this.walletOwners.length; i++) {
            if (this.walletOwners[i].id === walletOwnerId) {
                return this.walletOwners[i];
            }
        }
        return null;
    }

    public getCurrentURL() {
        return this.router.url;
    }

    public prepareUserAction(): boolean {
        const currentURL = this.getCurrentURL();
        if (currentURL.indexOf('/add') > -1) {
            return false;
        } else if (currentURL.indexOf('/edit') > -1) {
            return true;
        }
        return null;
    }
}
