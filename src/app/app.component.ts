import { Component, OnInit, OnDestroy} from '@angular/core';
import { ConfigService } from './shared/services/config.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'app';
  subscription:Subscription;

  constructor(private translateService : TranslateService ,private router:Router){
      
    this.translateService.setDefaultLang('en'); 
  } 

  ngOnInit(){
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    )
    .subscribe(() => window.scroll(0,0))
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
