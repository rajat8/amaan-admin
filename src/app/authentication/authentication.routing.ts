import { Routes } from '@angular/router';

import { NotFoundComponent } from './404/not-found.component';
import { LoginComponent } from './login/login.component';
import { ChangePasswordComponent } from './changepassword/change-password.component';
import { AuthGuard } from '../guards/auth.guard';

export const AuthenticationRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '404',
        canActivate : [AuthGuard],
        component: NotFoundComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'change-password',
        canActivate : [AuthGuard],
        component: ChangePasswordComponent
      }
    ]
  }
];
