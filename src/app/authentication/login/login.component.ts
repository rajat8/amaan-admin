import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { LoginModel } from './login.model';
import { Router, ActivatedRoute, UrlTree, UrlSegmentGroup, PRIMARY_OUTLET, UrlSegment} from '@angular/router';
import { LoginLogoutAuthenticationService } from '../../shared/services/loginlogoutauthentication.service';
import { LoginRequestModel } from './loginrequest.model';
import { formatDate } from '@angular/common';
import { MasterDataService } from '../../shared/services/masterdataservice.service';
import { MasterDataRequestModel } from './masterdatarequest.model';
import { API_URLs } from '../../shared/models/constants';
import { ConfigService } from '../../shared/services/config.service';
import { LogoutRequestModel } from '../../shared/models/logoutrequest.model';
import { LogoutModel } from '../../shared/models/logout.model';
import { EncryptData } from '../../shared/services/encryptdata.service';
import { SessionMgtService } from '../../shared/services/SessionMgt.service';


//declare var require: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[SessionMgtService]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errorMessage: String;
  language: string;
  error = '';
  loginModel: LoginModel = new LoginModel();
  loginrequest: LoginRequestModel = new LoginRequestModel();
  logindateandtime: string;
  masterdatarequest: MasterDataRequestModel = new MasterDataRequestModel();
  languageresponsedata: any[] = [{"languageId":1,"languageCode":"EN","languageName":"English","statusId":1}];  
  languagevalue: string = "en";
  requestparam: {};
  password: string;  
  default: string = "Select";
  logoutrequest : LogoutRequestModel = new LogoutRequestModel();
  logoutdata : LogoutModel = new LogoutModel();
  
  constructor(private apiurls: API_URLs, private translateService: TranslateService, private router: Router,
    private formBuilder: FormBuilder, private loginauthenticationservice: LoginLogoutAuthenticationService, 
    private masterdataservice: MasterDataService, public configService: ConfigService, 
    private translateservice: TranslateService , private encryptionService : EncryptData,
    private sessionSerivce:SessionMgtService) {
  }

  
  ngOnInit() {
   
    
    sessionStorage.clear();    
    this.buildForm();
    //this.router.navigate(["/dashboard/dashboard1"], { queryParams: { language: this.languagevalue } });
  }

  buildForm(): void {
    this.loginForm = this.formBuilder.group({
      'loginId': ['', Validators.required],
      'password': ['', Validators.required],
      'language': ['1']
      //'language': ['en']
    });
    
  }

  switchLanguage(language) {
    console.log("language value : " + language);
    this.languagevalue = language.toLowerCase();
    this.translateService.use(this.languagevalue);
  }

  login() {
    this.router.navigate(["/dashboard/dashboard1"], { queryParams: { language: this.languagevalue } });
    console.log("loginId: " + this.loginForm.value.loginId + ", password: " + this.loginForm.value.password + ", language: " + this.loginForm.value.language);

    if (this.loginForm.value.loginId == '' || this.loginForm.value.password == '') {      
      this.translateservice.get('err_login').subscribe((text: string) => { this.errorMessage = text; });
      return;
    }

    if(this.loginForm.value.language == "-1"){
      this.translateservice.get('err_login_language').subscribe((text: string) => { this.errorMessage = text; });
      return;
    }

    var encryptedPass = this.encryptionService.encryptData(this.loginForm.value.password).toUpperCase();
     console.log("encrypt pass : "+encryptedPass);
    //  this.encryptionService.set(this.loginForm.value.password);
   
    this.loginModel.loginId = this.loginForm.value.loginId;
    this.loginModel.password = this.encryptionService.encryptData(this.loginForm.value.password).toUpperCase();
    this.loginModel.languageCode = this.loginForm.value.language;
    this.loginModel.channel = "IWP";
    this.loginModel.comments = "User Login request";
    this.loginModel.requestcts = formatDate(new Date(),"yyyy-MM-dd HH:mm:ss.SSS", "en");
    this.loginModel.userLoginType = "INT";
   
    this.loginrequest.action = "LOGIN";
    this.loginrequest.request = this.loginModel;
    

    console.log("login request : "+JSON.stringify(this.loginrequest));

    this.loginauthenticationservice.logIn(this.apiurls.URL_LOGIN, this.loginrequest)
      .then(
        data => {          
          if (data == null) {
            this.translateservice.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
          } else if (data["response"]["resultCode"] == this.apiurls.RESULT_CODE) {

             console.log("LOGIN Response : " + JSON.stringify(data));
            sessionStorage.setItem("currentuser", data.response.loginId);
            sessionStorage.setItem("userId", data.response.userId);
            sessionStorage.setItem("userName", data.response.userName);
            sessionStorage.setItem("levelList", JSON.stringify(data.response.levelList));
            console.log("levelist : "+JSON.stringify(data.response.levelList));
            
                    
            if (data.response.resultCode == this.apiurls.RESULT_CODE && data.response.passwordRestRequired == 1) {
              this.router.navigate(["/authentication/change-password"], { queryParams: { language: this.languagevalue } });
              console.log("Forcefully change password.." + sessionStorage.getItem("loginId"));
            } else if (data.response.resultCode == this.apiurls.RESULT_CODE && data.response.passwordRestRequired == 0) {     
              this.masterAfterLogin();              
            } else {
              this.errorMessage = data.response.resultCodeDesc;
            }
          } else {
            this.errorMessage = "Error Code : " + data["response"]["resultCode"] + ": Error Desc : " + data["response"]["resultCodeDesc"];            
          }
        }
      );
  }

  masterAfterLogin(){
    this.masterdatarequest.action = "MASTERDATA";
    this.masterdatarequest.request = {};
    console.log("MASTERDATA Request : " + JSON.stringify(this.masterdatarequest));
  //  console.log("SavedServerPath: " + this.configService.getURL());

    this.masterdataservice.getData(this.apiurls.URL_MASTERDATA, this.masterdatarequest)
      .then(data => {
        //console.log("MASTERDATA Response : "+JSON.stringify(data));

        if (data == null) {
          this.translateservice.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
        } else if (data == "error") {
          this.translateService.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
        } else if (data["response"]["resultCode"] == this.apiurls.RESULT_CODE) {
          console.log("Master data successfully loaded");
          this.masterdataservice.getSysytemConfiguration().forEach(element => {
            if(element["configurationCode"] == "BROWSER_SESSION_TIMEOUT"){
              sessionStorage.setItem("BROWSER_SESSION_TIMEOUT",element["defaultValue"]);
              let time:Date=new Date();
              time.setMinutes(time.getMinutes()+Number(element["defaultValue"]));
              sessionStorage.setItem("SESSION_START_TIME",time.getTime().toString());
            }
          });
          
          this.router.navigate(["/dashboard/dashboard1"], { queryParams: { language: this.languagevalue } });
          
         } else {
          this.errorMessage = "Failed Error Code : " + data["response"]["resultCode"] + " : Error Desc : " + data["response"]["resultCodeDesc"];
        }
      });
  }

}
