import { Component , OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ChangePasswordValidator } from './change-password.validator';
import { Router , ActivatedRoute} from '@angular/router';
import { ChangePasswordRequestModel } from '../../shared/models/change-password-request.model';
import { ChangePasswordAuthenticationService } from '../../shared/services/changepasswordauthentication.service';
import { first } from 'rxjs/operators';
import {formatDate} from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { API_URLs } from '../../shared/models/constants';
import { ChangePasswordModel } from '../../shared/models/change-password.model';
import { MasterDataRequestModel } from '../login/masterdatarequest.model';
import { ConfigService } from '../../shared/services/config.service';
import { MasterDataService } from '../../shared/services/masterdataservice.service';
import { EncryptData } from '../../shared/services/encryptdata.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})

export class ChangePasswordComponent implements OnInit{

  changePasswordFormGroup : FormGroup;
  changepasswordmodel : ChangePasswordModel = new ChangePasswordModel();
  changepasswordrequest : ChangePasswordRequestModel = new ChangePasswordRequestModel();
  error = "";
  changepassworddateandtime : string;
  errorMessage : string;
  languagecode : string;
  //passwordpattern : "/^(?=.[A-Za-z])(?=.\d)[A-Za-z\d]{4,20}/";
  masterdatarequest: MasterDataRequestModel = new MasterDataRequestModel();

  submitted = false;
  constructor(private formBuilder: FormBuilder , private router: Router , private changepasswordauthenticationservice : ChangePasswordAuthenticationService,
  private translateservice : TranslateService , private activatedrouter : ActivatedRoute, private apiurls: API_URLs,private configService:ConfigService,private masterdataservice:MasterDataService , private encryptionService : EncryptData) {
  }
  ngOnInit(){

    this.activatedrouter.queryParams.subscribe(params =>{
      console.log("language : "+params.language);
      this.languagecode = params.language;
      this.translateservice.use(params.language);
      });

    this.changePasswordFormGroup = this.formBuilder.group({
      password : ['',[Validators.required , Validators.pattern('((?=.*\\d){2}(?=(.*[A-Z]){2})(?=.*[!@#$%^*:?()\\-`.+,/\]).{10,15})')]],
      newPassword : ['',[Validators.required , Validators.pattern('((?=.*\\d){2}(?=(.*[A-Z]){2})(?=.*[!@#$%^*:?()\\-`.+,/\]).{10,15})')]],
      confirmPassword : ['',Validators.required]
    },{
      validator: ChangePasswordValidator.validate.bind(this)
    })
  }

  get f() { return this.changePasswordFormGroup.controls; }
  

  onSubmit() {
      this.submitted = true;
      this.changepassworddateandtime = formatDate(new Date() ,  "yyyy-mm-dd HH:mm:ss" , "en");
      this.changepasswordmodel.password = this.encryptionService.encryptData(this.changePasswordFormGroup.value.password).toUpperCase();;
      this.changepasswordmodel.newPassword = this.encryptionService.encryptData(this.changePasswordFormGroup.value.newPassword).toUpperCase();;
      this.changepasswordmodel.confirmPassword = this.encryptionService.encryptData(this.changePasswordFormGroup.value.confirmPassword).toUpperCase();;
      this.changepasswordmodel.loginId = sessionStorage.getItem("currentuser");
      this.changepasswordmodel.userId = sessionStorage.getItem("userId");

      this.changepasswordmodel.channel = "IWP";
      this.changepasswordmodel.comments = "Change Pass Request";
      this.changepasswordmodel.requestcts = this.changepassworddateandtime;

      this.changepasswordrequest.action = "CHANGEPASSWORD";
      this.changepasswordrequest.request = this.changepasswordmodel;


      console.log("clicked");

      // stop here if form is invalid
      if(this.changePasswordFormGroup.invalid) {
          return;
      }

      console.log("ChangePassword request : "+JSON.stringify( this.changepasswordrequest));
      
    this.changepasswordauthenticationservice.changePassword(this.apiurls.URL_CHANGEPASSWORD, this.changepasswordrequest)
    .then(
      data => {
        console.log("data : " + JSON.stringify(data));
        if (data == null) {
          this.translateservice.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
        } else if (data["response"]["resultCode"] == this.apiurls.RESULT_CODE) {
          
          console.log("ChangePassword response : "+JSON.stringify(data));
          if(data.response.resultCode == this.apiurls.RESULT_CODE){
            console.log("languagecode : "+this.languagecode);
            this.masterAfterChangePassword();            
          }else{
            this.errorMessage = data.response.resultCodeDesc;
          }

        } else {
          this.errorMessage = "Failed : Error Code : " + data["response"]["resultCode"] + " : Error Desc : " + data["response"]["resultCodeDesc"];
        }
      }
    );
  }

  masterAfterChangePassword(){
    this.masterdatarequest.action = "MASTERDATA";
    this.masterdatarequest.request = {};
    console.log("MASTERDATA Request : " + JSON.stringify(this.masterdatarequest));
   // console.log("SavedServerPath: " + this.configService.getURL());

    this.masterdataservice.getData(this.apiurls.URL_MASTERDATA, this.masterdatarequest)
      .then(data => {
        //console.log("MASTERDATA Response : "+JSON.stringify(data));

        if (data == null) {
          this.translateservice.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
        } else if (data == "error") {
          this.translateservice.get('common_error_message').subscribe((text: string) => { this.errorMessage = text; });
        } else if (data["response"]["resultCode"] == this.apiurls.RESULT_CODE) {
          console.log("Master data successfully loaded");
          this.router.navigate(["/dashboard/dashboard1"],{queryParams:{language:this.languagecode}});          
         } else {
          this.errorMessage = "Failed Error Code : " + data["response"]["resultCode"] + " : Error Desc : " + data["response"]["resultCodeDesc"];
        }
      });
  }
}
