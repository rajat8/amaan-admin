
import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';
 

export const Approutes: Routes = [
  {
    path: '', component: FullComponent,
    children: [
      { path: '', redirectTo: '/authentication/login', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: () => import('./dashboards/dashboard.module').then(m => m.DashboardModule) },
      { path: 'component', loadChildren: () => import('./component/component.module').then(m => m.ComponentsModule) },
      { path: 'icons', loadChildren: () => import('./icons/icons.module').then(m => m.IconsModule) },
      { path: 'forms', loadChildren: () => import('./form/forms.module').then(m => m.FormModule) },
      { path: 'charts', loadChildren: () => import('./charts/charts.module').then(m => m.ChartModule) },
      { path: 'widgets', loadChildren: () => import('./widgets/widgets.module').then(m => m.WidgetsModule) },
      { path: 'apps', loadChildren: () => import('./apps/apps.module').then(m => m.AppsModule) },
      { path: 'sample-pages', loadChildren: () => import('./sample-pages/sample-pages.module').then(m => m.SamplePagesModule) },
      { path: 'password' ,loadChildren:() => import('./changepassword/changepassword.module').then(m => m.ChangepasswordModule)},
      { path: 'systemconfiguration',loadChildren:() => import('./system-configuration/system-configuration.module').then(m => m.SystemConfigurationModule)},
      { path: 'wallet-owner',loadChildren:() => import('./wallet-owner/wallet-owner.module').then(m => m.WalletOwnerModule)},
      { path: 'rolepermission',loadChildren:() => import('./rolepermission/rolepermission.module').then(m => m.RolePermissionModule)},
      { path: 'rolegroup',loadChildren:() => import('./rolegroup/rolegroup.module').then(m => m.RoleGroupModule)},
      {path : 'user', loadChildren:() => import ('./user/user.module').then(m => m.UserModule)}
    ]
  },
  {
    path: '', component: BlankComponent,
    children: [
      {
        path: 'authentication',
        loadChildren: () => import('./authentication/authentication.module').then(m => m.AuthenticationModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/authentication/404'
  }
];
