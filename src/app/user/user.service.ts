import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModel } from './user.model';
import { Router } from '@angular/router';

@Injectable()
export class UserService {

    constructor(private router: Router) { }

    private userModel: UserModel[];
 

    public fetchSystemUserList(): UserModel[] {
        const systemUsers: UserModel[] = [

       
            {
                id	 : 1,
                code	: '00001' ,
                walletownergroup_code	 :'WC001' ,
                rolegroup_code	 :'RC0001', 
                user_name	 :'Name1' ,
                alias_name	 :'alias_name1', 
                national_id	 :101 ,
                mobile_number: 9990009090,	 
                hr_id	:1001 ,
                bob_title	 :'Title1' ,
                department_name	:'Department1' , 
                status	 :'Active' ,
                state	 :'Haryana', 
                creation_date	: new Date(), 
                created_by	 :'Administrator' ,
                updation_date	 : new Date(),
                updated_by	 :'Administrator'
                
            },
            {
                id	 : 2,
                code	: '00002' ,
                walletownergroup_code	 :'WC002' ,
                rolegroup_code	 :'RC0002', 
                user_name	 :'Name2' ,
                alias_name	 :'alias_name2', 
                national_id	 :102 ,
                mobile_number: 9990009091,	 
                hr_id	:1002 ,
                bob_title	 :'Title2' ,
                department_name	:'Department2' , 
                status	 :'Active' ,
                state	 :'Gujrat', 
                creation_date	: new Date(), 
                created_by	 :'Administrator' ,
                updation_date	 : new Date(),
                updated_by	 :'Administrator'
                
            },
            {
                id	 : 3,
                code	: '00003' ,
                walletownergroup_code	 :'WC003' ,
                rolegroup_code	 :'RC0003', 
                user_name	 :'Name3' ,
                alias_name	 :'alias_name3', 
                national_id	 :103 ,
                mobile_number: 9990009093,	 
                hr_id	:1003 ,
                bob_title	 :'Title3' ,
                department_name	:'Department3' , 
                status	 :'Inactive' ,
                state	 :'Maharastra', 
                creation_date	: new Date(), 
                created_by	 :'Administrator' ,
                updation_date	 : new Date(),
                updated_by	 :'Administrator'
                
            }
             
            
            
        ];

        this.userModel = systemUsers;
        return systemUsers;
    }

    public geSystemUserById(systemUserId: number): UserModel {
        for (let i = 0; i < this.userModel.length; i++) {
            if (this.userModel[i].id === systemUserId) {
                return this.userModel[i];
            }
        }
       // return null;
    }

    public getCurrentURL() {
        return this.router.url;
    }

    public prepareUserAction(): boolean {
        const currentURL = this.getCurrentURL();
        if (currentURL.indexOf('/add') > -1) {
            return false;
        } else if (currentURL.indexOf('/edit') > -1) {
            return true;
        }
        return null;
    }
   
 

    
}
