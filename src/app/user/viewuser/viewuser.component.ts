import { Component, OnInit } from '@angular/core';
import { UserModel } from '../user.model';
import { UserService } from '../user.service';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.css']
})
export class ViewuserComponent implements OnInit {

 
  editMode = false;
  paramList :any;
  userModel: UserModel;
  id : number;
  code	:String ;
  walletownergroup_code	 :String ;
  rolegroup_code	 :String ;
  user_name	 :String ;
  alias_name	 :String ;
  national_id	 :number ;
  mobile_number: number	 ;
  hr_id	:number ;
  bob_title	 :String ;
  department_name	:String ; 
  status	 :String ;
  state	 :String ;
  creation_date	: Date ;
  created_by	 :String ;
  updation_date	 :Date;
  updated_by	 :String ;

  constructor(private userService: UserService,private formBuilder: FormBuilder, private router: Router, private activatedrouter: ActivatedRoute,
    private route: ActivatedRoute) { }

   
  

  ngOnInit() {

      this.route.params.subscribe((params: Params) => {
       
    
      if(params.id) {
      console.log("params" + params.id);
        this.userModel = this.userService.geSystemUserById(+params.id);
        this.id = this.userModel.id;
        this. code	= this.userModel.code;
        this.walletownergroup_code	= this.userModel.walletownergroup_code;;
        this.rolegroup_code	= this.userModel.rolegroup_code;
        this.user_name	= this.userModel.user_name;
        this.alias_name	 = this.userModel.alias_name;
        this.national_id	= this.userModel.national_id;
        this. mobile_number= this.userModel.mobile_number;
        this.hr_id= this.userModel.hr_id;
        this.bob_title= this.userModel.bob_title;
        this.department_name= this.userModel.department_name;
        this.status	= this.userModel.status;
        this.state	= this.userModel.state;
        this.creation_date= this.userModel.creation_date;
        this.created_by	= this.userModel.created_by;
        this.updation_date	= this.userModel.updation_date;
        this.updated_by	 = this.userModel.updated_by;
      }
     
       
    });
  }

  navigateToSytemUserCreation(){
    
      this.router.navigate( ['../../'], {relativeTo: this.route} );
    
  }
   

  onCancel() {
    
  
      this.router.navigate( ['../../'], {relativeTo: this.route} );
     
  }

}
