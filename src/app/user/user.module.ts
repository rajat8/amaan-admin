import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateuserComponent } from './createuser/createuser.component';
import { UserRoutingModule } from './user-routing.module';
import { UserService } from './user.service';
import { UserComponent} from './user.component';
import { ViewuserComponent } from './viewuser/viewuser.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [ UserComponent,CreateuserComponent,ViewuserComponent],
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      UserRoutingModule,
    ],
    providers: [
      UserService
    ]
  })
export class UserModule{}