import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
 
import { UserComponent } from './user.component';
import { CreateuserComponent } from './createuser/createuser.component';
import { ViewuserComponent } from './viewuser/viewuser.component';
const routes: Routes = [
    { path: '', component: UserComponent, pathMatch: "full" },
    { path: 'add', component: CreateuserComponent },
    { path: 'view/:id', component: ViewuserComponent },
    { path: 'edit/:id', component: CreateuserComponent }
  ];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class UserRoutingModule{}