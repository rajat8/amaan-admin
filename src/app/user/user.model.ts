export interface UserModel {

    id: number;
    code: String;
    walletownergroup_code: String;
    rolegroup_code: String;
    user_name: String;
    alias_name: String;
    national_id: number;
    mobile_number: number;
    hr_id: number;
    bob_title: String;
    department_name: String;
    status: String;
    state: String;
    creation_date: Date;
    created_by: String;
    updation_date: Date;
    updated_by: String;
}