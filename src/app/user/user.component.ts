import { Component, OnInit } from '@angular/core';
import { UserModel } from './user.model';
import { UserService } from './user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  private users: UserModel[];
  constructor(private userService: UserService,
    private route: ActivatedRoute,
    private router: Router) { 
  }

  ngOnInit() {
      this.users = this.userService.fetchSystemUserList();
  }

  viewUser(userid: number) {

    this.router.navigate( ['view', userid], {relativeTo: this.route} );
  }

  editUser(userid: any) {
    this.router.navigate( ['edit', userid], {relativeTo: this.route} );

  }
}
